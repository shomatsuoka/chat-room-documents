# Chat Room - チャットルームにようこそ（Documents）


### ◉ Description (English)
The designs and mockups used to develop the source code below are stored in this repository.

Source code:
https://gitlab.com/shomatsuoka/chat-room

---

### ◉ ご説明（日本語）
下記のソースコードの開発に使用したデザインとモックアップをこのリポジトリに格納いたしました。

ソースコード:
https://gitlab.com/shomatsuoka/chat-room